using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestAPI.Dtos.TaskDtos;
using RestAPI.Models;

namespace RestAPI.Helpers
{
    public class TaskToDtoConverter
    {
        public static Models.Task TaskCrateDtoToTask(TaskCreateDto taskCreate)
        {
            var newTask = new Models.Task();
            newTask.Tittle = taskCreate.Title;
            newTask.Description = taskCreate.Description;
            newTask.CreatedDate = DateTime.Now;
            newTask.Status = true;  
            newTask.Priority = taskCreate.Priority;
            newTask.UserId = taskCreate.UserId;
            newTask.GroupId = taskCreate.GroupId;
            return newTask;
        }

        public static TaskReturnDto TaskToTaskReturnDto(Models.Task task)
        {
            TaskReturnDto taskReturn = new TaskReturnDto();
            taskReturn.Id = task.Id;
            taskReturn.Tittle = task.Tittle;
            taskReturn.Description = task.Description;
            taskReturn.CreatedDate = task.CreatedDate;
            taskReturn.Status = task.Status;
            taskReturn.FinishDate = task.FinishDate;
            taskReturn.Priority = task.Priority;
            taskReturn.UserId = task.UserId;
            taskReturn.GroupId = task.GroupId;
            return taskReturn; 
        }

        public static History TaskToRecord(Models.Task task, string affectation)
        {
            History record = new History();
                record.IdAffectedEntity = task.Id;
                record.AffectedEntity = task.Tittle;
                record.Affectation = affectation;
                record.Date = DateTime.Now;
            return record;
        }

        
    }
}