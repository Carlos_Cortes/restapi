using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestAPI.Dtos.UserDtos;
using RestAPI.Models;

namespace RestAPI.Helpers
{

    public class UserToDtoConverter
    {
        public static User UserCreateDtoToUser (UserCreateDto userDto)
        {
            User newUser = new User ();
            newUser.Name = userDto.Name;
            newUser.Password = userDto.Password;
            newUser.CreatedDate = DateTime.Now;
            newUser.Status = true;
            return newUser;
        }
        public static UserReturnDto UserToUserReturn (User user)
        {
            UserReturnDto userReturn = new UserReturnDto();
                userReturn.Id = user.Id;
                userReturn.Name = user.Name;
                userReturn.CreatedDate = user.CreatedDate;
                userReturn.Status = user.Status;
            return userReturn;
        }

        public static UserDeleteDto UserToUserDeleteDto(User user)
        {
            UserDeleteDto userDelete = new UserDeleteDto();
                userDelete.Id = user.Id;
                userDelete.Name = user.Name;
                userDelete.Status = "User Deleted";
            return userDelete;
        }

        public static History UserToRecord(User user, String affectation)
        {
            History record = new History();
                record.IdAffectedEntity = user.Id;
                record.AffectedEntity = "User";
                record.Affectation = affectation;
                record.Date = DateTime.Now;
            return record;
        }
    }
}