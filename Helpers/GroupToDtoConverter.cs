using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using RestAPI.Dtos.GroupDtos;
using RestAPI.Models;

namespace RestAPI.Helpers
{
    public class GroupToDtoConverter
    {

        public static Models.Group GroupDtoTo(GroupCreateDto groupDto)
        {
            Models.Group group = new Models.Group();
            group.Tittle = groupDto.Title;
            group.UserId = groupDto.UserId;
            return group;
        }

        public static GroupReturnDto GroupToGroupReturnDto(Models.Group group)
        {
            GroupReturnDto returnDto= new GroupReturnDto();
            returnDto.Id = group.Id;
            returnDto.Tittle = group.Tittle;
            returnDto.UserId = group.UserId;
            return returnDto;
        }
    }
}