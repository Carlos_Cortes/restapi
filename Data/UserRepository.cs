using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RestAPI.Data.Interfaces;
using RestAPI.Dtos.UserDtos;
using RestAPI.Models;

namespace RestAPI.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _dataContext;
        public UserRepository(DataContext context)
        {
            _dataContext = context;
        }

        public void Add<User>(User user)
        {
            _dataContext.Add(user);
        }

        public void Delete<User>(User user)
        {
            _dataContext.Remove(user);
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            var users = await _dataContext.Users.ToListAsync();
            return users;
        }

        public async Task<User> GetUserByIdAsync(Guid id)
        {
            var user = await _dataContext.Users.FirstOrDefaultAsync(x=>x.Id==id);
            return user;
        }

        public async Task<bool> SaveAll()
        {
            return await _dataContext.SaveChangesAsync() > 0;
        }

    }
}