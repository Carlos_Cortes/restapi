using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RestAPI.Data.Interfaces;
using RestAPI.Models;

namespace RestAPI.Data
{
    public class HistoryRepository : IHistoryRepository
    {
        private readonly DataContext _dataContext;
        public HistoryRepository(DataContext context)
        {
            _dataContext = context;
        }
        public void Add<History>(History history)
        {
            _dataContext.Add(history);
        }

        public async Task<IEnumerable<History>> GetRecordAsync()
        {
            var records = await _dataContext.History.ToListAsync();
            return records;
        }

        public async Task<IEnumerable<History>> GetRecordByTaskId(Guid id)
        {
            var record = await _dataContext.History.Where(x => x.IdAffectedEntity == id).ToListAsync();
            return record;
        }

        public async Task<bool> SaveAll()
        {
            return await _dataContext.SaveChangesAsync() > 0;
        }

    
    }
}