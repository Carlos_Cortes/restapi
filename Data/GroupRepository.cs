using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RestAPI.Data.Interfaces;
using RestAPI.Models;

namespace RestAPI.Data
{
    public class GroupRepository : IGroupRepository
    {
        private readonly DataContext _dataContext;
        public GroupRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Add<Group>(Group group)
        {
            _dataContext.Add(group);
        }

        public void Delete<Group>(Group group)
        {
            _dataContext.Remove(group);
        }

        public async Task<Group> GetGroupByIdAsync(Guid id)
        {
            var group = await _dataContext.Groups.FirstOrDefaultAsync(x=>x.Id == id);
            return group;
        }

        public async Task<Group> GetGroupByTittleAsync(string tittle)
        {
            var group = await _dataContext.Groups.FirstOrDefaultAsync(x=>x.Tittle == tittle);
            return group;
        }

        public async Task<IEnumerable<Group>> GetGroupsAsync()
        {
            var groups = await _dataContext.Groups.ToListAsync();
            return groups;
        }

        public async Task<IEnumerable<Group>> GetGroupsByUserIdAsync(Guid id)
        {
            var groups = await _dataContext.Groups.Where(x => x.UserId.Equals(id)).ToListAsync();
            return groups;
        }

        public async Task<bool> SaveAll()
        {
            return await _dataContext.SaveChangesAsync() > 0;
        }
    }
}