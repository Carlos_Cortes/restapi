using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RestAPI.Data.Interfaces;

namespace RestAPI.Data
{
    public class TaskRepository : ITaskRepository
    {
        private readonly DataContext _dataContext;
        public TaskRepository(DataContext context)
        {
            _dataContext = context;
        }

        public void Add<Task>(Task task)
        {
            _dataContext.Add(task);
        }

        public void Delete<Task>(Task task)
        {
            _dataContext.Remove(task);
        }

        public async Task<IEnumerable<Models.Task>> GetTasksAsync()
        {
            var tasks = await _dataContext.Tasks.ToListAsync();
            return tasks;
        }

        public async Task<Models.Task> GetTaskByIdAsync(Guid id)
        {
            var task = await _dataContext.Tasks.FirstOrDefaultAsync(x => x.Id == id);
            return task;
        }

        public async Task<bool> SaveAll()
        {
            return await _dataContext.SaveChangesAsync() > 0;
        }

        public Task<IEnumerable<Models.Task>> UpdateTaskByIdAsync(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Models.Task>> GetTasksByHighPriority()
        {
            var tasks = await _dataContext.Tasks
                                  .OrderByDescending(x => x.Priority)
                                  .ToListAsync();
            return tasks;
        }

        public async Task<IEnumerable<Models.Task>> GetTasksByLowPriority()
        {
            var tasks = await _dataContext.Tasks
                                  .OrderBy(x => x.Priority)
                                  .ToListAsync();
            return tasks;
        }

        public async Task<IEnumerable<Models.Task>> GetTasksByUserId(Guid id)
        {
            var tasks = await _dataContext.Tasks.Where(x => x.User.Id == id).ToListAsync();
            return tasks;
        }

        public async Task<IEnumerable<Models.Task>> GetTasksByGroupId(Guid id)
        {
            var tasks = await _dataContext.Tasks.Where(x => x.Group.Id == id).ToListAsync();
            return tasks;
        }

        public async Task<IEnumerable<Models.Task>> GetTasksByPropity(int priority)
        {
            var tasks = await _dataContext.Tasks.Where(x => x.Priority == priority).ToListAsync();
            return tasks;
        }

        public Task<IEnumerable<Models.Task>> UpdateTaskByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}