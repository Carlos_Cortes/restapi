using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestAPI.Models;

namespace RestAPI.Data.Interfaces
{
    public interface IUserRepository
    {
        void Add<User>(User user);
        void Delete<User>(User user);
        Task<bool> SaveAll();
        Task<IEnumerable<User>> GetUsersAsync();
        Task<User> GetUserByIdAsync(Guid id);

    }
}