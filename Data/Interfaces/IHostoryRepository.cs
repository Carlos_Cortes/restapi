using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestAPI.Models;

namespace RestAPI.Data.Interfaces
{
    public interface IHistoryRepository
    {
        void Add<Record>(Record record);
        Task<bool> SaveAll();
        Task<IEnumerable<History>> GetRecordAsync();
        Task<IEnumerable<History>> GetRecordByTaskId(Guid id);
    }
}