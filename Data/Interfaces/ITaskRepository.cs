using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestAPI.Models;

namespace RestAPI.Data.Interfaces
{
    public interface ITaskRepository
    {
        void Add<Task>(Task task);
        void Delete<Task>(Task task);
        Task<bool> SaveAll();
        Task<IEnumerable<Models.Task>> GetTasksAsync();
        Task<IEnumerable<Models.Task>> GetTasksByHighPriority();
        Task<IEnumerable<Models.Task>> GetTasksByLowPriority();
        Task<Models.Task> GetTaskByIdAsync(Guid id);
        Task<IEnumerable<Models.Task>> GetTasksByUserId(Guid id);
        Task<IEnumerable<Models.Task>> GetTasksByGroupId(Guid id);
        Task<IEnumerable<Models.Task>> GetTasksByPropity(int priority);
        Task<IEnumerable<Models.Task>> UpdateTaskByIdAsync(Guid id);
    }
}