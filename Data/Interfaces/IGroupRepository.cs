using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestAPI.Models;

namespace RestAPI.Data.Interfaces
{
    public interface IGroupRepository
    {
        void Add<Group>(Group group);
        void Delete<Group>(Group group);
        Task<bool> SaveAll();
        Task<IEnumerable<Group>> GetGroupsAsync();
        Task<Group> GetGroupByIdAsync(Guid id);
        Task<Group> GetGroupByTittleAsync(string tittle);
        Task<IEnumerable<Group>> GetGroupsByUserIdAsync(Guid id);
       
    }
}