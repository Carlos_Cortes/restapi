using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using RestAPI.Data.Interfaces;
using RestAPI.Dtos.TaskDtos;
using RestAPI.Helpers;
using RestAPI.Models;

namespace RestAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskController : ControllerBase 
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IHistoryRepository _recordRepository;

        public TaskController(ITaskRepository taskRepository, IHistoryRepository recordRepository)
        {
            _taskRepository = taskRepository;
            _recordRepository = recordRepository;
        }

        [HttpPost("create-task")]
        public async Task<IActionResult> Post(TaskCreateDto taskDto)
        {
            var newTask = TaskToDtoConverter.TaskCrateDtoToTask(taskDto);
            _taskRepository.Add(newTask);
            if(await _taskRepository.SaveAll())
            {
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(newTask);
                return Ok(taskReturn);
            }else{
                return BadRequest();
            }
        }

        [HttpDelete("delete-task-by-id/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var task = await _taskRepository.GetTaskByIdAsync(id);
            if(task == null)
            {
                return NotFound();
            }
            _taskRepository.Delete(task);
            if(await _taskRepository.SaveAll())
            {
                return Ok(task);
            }else
            {
                return BadRequest();
            }
        }

        [HttpGet("get-tasks")]
        public async Task<IActionResult> GetTasks()
        {
            var task = await _taskRepository.GetTasksAsync();
            List<TaskReturnDto> tasksReturn = new List<TaskReturnDto>();
            foreach(var e in task)
            {
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(e);
                tasksReturn.Add(taskReturn);
            }
            return Ok(tasksReturn);
        }

        [HttpGet("get-task-by-id/{id}")]
        public async Task<IActionResult> GetTaskById(Guid id)
        {
            var task = await _taskRepository.GetTaskByIdAsync(id);
            TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(task);
            return Ok(taskReturn);
        }

        [HttpGet("get-task-by-high-priority")]
        public async Task<IActionResult> GetTasksByHighPriority()
        {
            var tasks = await _taskRepository.GetTasksByHighPriority();
            List<TaskReturnDto> tasksReturn = new List<TaskReturnDto>();
            foreach(var e in tasks)
            {
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(e);
                tasksReturn.Add(taskReturn);
            }
            return Ok(tasksReturn);
        }

        [HttpGet("get-tasks-by-low-priority")]
        public async Task<IActionResult> GetTasksByLowPriority()
        {
            var tasks = await _taskRepository.GetTasksByLowPriority();
            List<TaskReturnDto> tasksReturn = new List<TaskReturnDto>();
            foreach(var e in tasks)
            {
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(e);
                tasksReturn.Add(taskReturn);
            }
            return Ok(tasksReturn);
        }

        [HttpGet("get-tasks-by-user-id/{id}")]
        public async Task<IActionResult> GetTasksByUserId(Guid id)
        {
            var tasks = await _taskRepository.GetTasksByUserId(id);
            List<TaskReturnDto> tasksReturn = new List<TaskReturnDto>();
            foreach(var e in tasks)
            {
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(e);
                tasksReturn.Add(taskReturn);
            }
            return Ok(tasksReturn);
        }

        [HttpGet("get-tasks-by-group-id/{id}")]
        public async Task<IActionResult> GetTasksByGroupId(Guid id)
        {
            var tasks = await _taskRepository.GetTasksByGroupId(id);
            List<TaskReturnDto> tasksReturn = new List<TaskReturnDto>();
            foreach(var e in tasks)
            {
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(e);
                tasksReturn.Add(taskReturn);
            }
            return Ok(tasksReturn);
        }

        [HttpGet("get-tasks-by-priority/{priority}")]
        public async Task<IActionResult> GetTasksByPriority(int priority)
        {
            var tasks = await _taskRepository.GetTasksByPropity(priority);
            List<TaskReturnDto> tasksReturn = new List<TaskReturnDto>();
            foreach(var e in tasks)
            {
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(e);
                tasksReturn.Add(taskReturn);
            }
            return Ok(tasksReturn);
        }

        [HttpPut("update-priority")]
        public async Task<IActionResult> PutNewPriority(TaskUpdatePriorityDto taskDto)
        {
            var newTask = await _taskRepository.GetTaskByIdAsync(taskDto.Id);
            if (newTask == null)
                return NotFound("The task you want to update does not exist");

            newTask.Priority = taskDto.Priority;
            if (await _taskRepository.SaveAll())
            {   
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(newTask);
                return Ok(taskReturn);
            }else 
            { 
                return BadRequest();
            }
            
        }

        [HttpPut("change-status/{id}")]
        public async Task<IActionResult> PutNewfinishDate(Guid id)
        {
            var newTask = await _taskRepository.GetTaskByIdAsync(id);
            if (newTask == null)
                return NotFound("The task you want to change status does't exist");
            
            if(newTask.Status == true){
                newTask.FinishDate  = DateTime.Now;
                newTask.Status = false;

            }else if(newTask.Status == false){
                newTask.FinishDate = null;
                newTask.Status = true;
            }
            
            if (await _taskRepository.SaveAll())
            {   
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(newTask);
                History record = new History();
                if(newTask.Status == true){
                    record = TaskToDtoConverter.TaskToRecord(newTask, "Open the task again");
                }else{
                    record = TaskToDtoConverter.TaskToRecord(newTask, "Close task");
                }
    
                _recordRepository.Add(record);
                await _taskRepository.SaveAll();
                return Ok(taskReturn);
            }else 
            { 
                return BadRequest("prueba 1");
            }
        }

        [HttpPut("update-tittle")]
        public async Task<IActionResult> PutNewTittle(TaskUpdateTittleDto taskDto)
        {
            var newTask = await _taskRepository.GetTaskByIdAsync(taskDto.Id);
            if (newTask == null)
                return NotFound("The task you want to update does not exist");

            newTask.Tittle = taskDto.Tittle;
            if (await _taskRepository.SaveAll())
            {   
                TaskReturnDto taskReturn = TaskToDtoConverter.TaskToTaskReturnDto(newTask);
                return Ok(taskReturn);
            }else 
            { 
                return BadRequest();
            }
        }

    }
}