using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestAPI.Data.Interfaces;
using RestAPI.Dtos.GroupDtos;
using RestAPI.Helpers;
using RestAPI.Models;

namespace RestAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GroupController : ControllerBase
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IUserRepository _userRepository;

        public GroupController(IGroupRepository groupRepository, IUserRepository userRepository)
        {
            _groupRepository = groupRepository;
            _userRepository = userRepository;
        }

        [HttpPost("create-group")]
        public async Task<IActionResult> Post(GroupCreateDto groupDto)
        {
            var newGroup = GroupToDtoConverter.GroupDtoTo(groupDto);
            _groupRepository.Add(newGroup);

            if(await _groupRepository.SaveAll())
            {
                GroupReturnDto returnDto = GroupToDtoConverter.GroupToGroupReturnDto(newGroup);
                return Ok(returnDto); 
            }else
            {
                return BadRequest();
            }
        }

        [HttpDelete("delete-group-by-id/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var group = await _groupRepository.GetGroupByIdAsync(id);
            if(group == null)
            {
                return NotFound();
            }
            _groupRepository.Delete(group);
            if(await _userRepository.SaveAll())
            {
                GroupReturnDto groupReturn = GroupToDtoConverter.GroupToGroupReturnDto(group);
                return Ok(groupReturn);
            }else
            {
                return BadRequest();
            }

        }

        [HttpGet("get-groups")]
        public async Task<IActionResult> Get()
        {
            var group = await _groupRepository.GetGroupsAsync();
            List<GroupReturnDto> groups = new List<GroupReturnDto>();
            foreach(var e in group)
            {
                GroupReturnDto groupReturnDto = GroupToDtoConverter.GroupToGroupReturnDto(e);
                groups.Add(groupReturnDto);
            }
            return Ok(groups);
        }

        [HttpGet("get-gruoups-by-user-id/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var group = await _groupRepository.GetGroupsByUserIdAsync(id);
            List<GroupReturnDto> groups = new List<GroupReturnDto>();
            foreach(var e in group)
            {
                GroupReturnDto groupReturn = GroupToDtoConverter.GroupToGroupReturnDto(e);
                groups.Add(groupReturn);
            }
            return Ok(groups);
        }

        [HttpGet("get-group-by-tittle/{tittle}")]
        public async Task<IActionResult> Get(string tittle)
        {
            var group = await _groupRepository.GetGroupByTittleAsync(tittle);
            GroupReturnDto groupReturn = GroupToDtoConverter.GroupToGroupReturnDto(group);
            return Ok(groupReturn);
        }
        
        [HttpGet("get-groups-by-id/{id}")]
        public async Task<IActionResult> GetGroupById(Guid id)
        {
            var group = await _groupRepository.GetGroupByIdAsync(id);
            GroupReturnDto groupReturn = GroupToDtoConverter.GroupToGroupReturnDto(group);
            return Ok(groupReturn);
        }

    }
}