using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RestAPI.Data.Interfaces;
using RestAPI.Dtos.UserDtos;
using RestAPI.Models;
using RestAPI.Helpers;

namespace RestAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost("create-user")]
        public async Task<IActionResult> Post(UserCreateDto userDto)
        {
            var newUser = UserToDtoConverter.UserCreateDtoToUser(userDto);
            _userRepository.Add(newUser);
            if (await _userRepository.SaveAll())
            {   
                UserReturnDto userReturn = UserToDtoConverter.UserToUserReturn(newUser);
                return Ok(userReturn);
            }else 
            { 
                return BadRequest();
            }
        }

        [HttpDelete("Detele-user-by-id/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var user = await _userRepository.GetUserByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _userRepository.Delete(user);
            if (await _userRepository.SaveAll())
            {   
                UserDeleteDto userDelete = UserToDtoConverter.UserToUserDeleteDto(user);

                return Ok(userDelete);
            }else 
            { 
                return BadRequest();
            }
        }

        [HttpGet("get-users")]
        public async Task<IActionResult> Get()
        {
            var user = await _userRepository.GetUsersAsync();
            List<UserReturnDto> usersReturn = new List<UserReturnDto>();
            foreach(var i in user)
            {
                UserReturnDto userReturn = UserToDtoConverter.UserToUserReturn(i);
                usersReturn.Add(userReturn);
            }
            return Ok(usersReturn);
        }

        [HttpGet("get-user-by-id/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var user = await _userRepository.GetUserByIdAsync(id);
            if (user == null)
            {
                return NotFound("The user does not exist");
            }
            UserReturnDto userReturn = UserToDtoConverter.UserToUserReturn(user);
            return Ok(userReturn);
        }

        [HttpPut("update-status-to-user")]
        public async Task<IActionResult> ChangeStatus(Guid id)
        {
            var newUser = await _userRepository.GetUserByIdAsync(id);
            if (newUser == null)
                return NotFound("The user you want to update does not exist");
            
            if(newUser.Status == true)
            {
                newUser.Status = false;
            }else
            {
                newUser.Status = true;
            }

            if (await _userRepository.SaveAll())
            {   
                UserReturnDto userReturn = UserToDtoConverter.UserToUserReturn(newUser);
                return Ok(userReturn);
            }else 
            { 
                return BadRequest();
            }
        }

    }
}