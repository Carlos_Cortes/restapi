using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestAPI.Data.Interfaces;

namespace RestAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HistoryController : ControllerBase
    {
        private readonly IHistoryRepository _recordRepository;

        public HistoryController(IHistoryRepository recordRepository)
        {
            _recordRepository = recordRepository;
        }

        [HttpGet("get-history")]
        public async Task<IActionResult> GetRecord()
        {
            var history = await _recordRepository.GetRecordAsync();
            return Ok(history);
        }

        [HttpGet("get-history-by-task-id/{id}")]
        public async Task<IActionResult> GetRecordByTaskId(Guid id)
        {
            var history = await _recordRepository.GetRecordByTaskId(id);
            return Ok(history);
        }
    }
}