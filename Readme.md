# About the C# Taks API:
Description:
The API works with an SQLite relational database, which does not need a container to run, making it easy to use.
It is possible to create users, tasks and task groups, in addition to having a history in which the status changes of the tasks can be viewed.
- Users have a name, password and status, the status can change between active and inactive.
- Task groups have a title and are assigned to a user; it is not possible to edit them, but you can delete them.
- Tasks have a title, description, creation date, end date (when you change the status of a task it will be automatically placed), priority level (being able to enter a value from 0 to 4), assigned user, and assigned 
group (optional), it is possible to edit their priority level, end date and title.
- In the history you will see the ID of the affected task, its name, the action that may be closing a task or opening it again and the exact date where this occurred.
-------------------------------
# How to Run the API on C#
## Requisites:
- Have dotnet version 8 or the most current installed on the PC.
- As an additional requirement, it is requested to have node js and npm installed to execute the database filling script.
If you are using Ubuntu you can install Node Js and npm with the following [tutorial](https://www.hostinger.com/tutorials/how-to-install-node-ubuntu?ppc_campaign=google_search_generic_hosting_all&bidkw=defaultkeyword&lo=1010200&gad_source=1&gclid=CjwKCAjw57exBhAsEiwAaIxaZhe5VVQk8HGCwB3z_07ki2HvshOStaZGniC250Us0jeXRld-FHXgfhoC-8kQAvD_BwE).

## Download from the repository
Within the repository we go to the "Code" section and copy the link to clone the repository.
![alt text](/Images/Repository.png)
z
Once the link has been copied, we go to our file explorer where the content of the repository will be downloaded, we will right-click on the desired location and select the "Open in a terminal" option to initialize a terminal.

nside the terminal we will write the command "git clone" adding the link that was copied to clone the repository.
```
git clone https://gitlab.com/Carlos_Cortes/restapi.git
```
It will only be a matter of entering the directory to be inside the repository, this is done with the command:
```
cd RestAPI/
```
Which would be the root directory of our API

## Execution:
It is recommended to use the terminal to execute the API, you can enter the file explorer, inside the root directory of the API and right click selecting the "Open in a terminal" option.

Before starting the API, the Docker compose container must be raised.
To do this, you must first move to the "Container" directory:
```
cd Container/
```
Once inside the directory, the following command must be executed to raise the container, inside it has an initial configuration which will make the MySQL database run on port 3308:
```
sudo docker compose up -d
```
The terminal should look like this: <br>:
![alt text](/Images/terminal1.png)

The API is created with "code first" so it is necessary to execute a database update command to create the tables:
```
dotnet ef database update
```
--------------------------------------------------------------------------------
NOTE: After these steps, the API can be executed correctly with the MySQL database, but it must be taken into account that it will be empty at the moment.
You must move to the root folder, you can do it with the following command:
```
cd RestAPI/
```

Once in the API root directory, run the command:
```
dotnet run
```

You can view in the browser using the following URL:
```
http://localhost:5135/swagger/index.html
```
You should see an image like the one below in your browser:

### Image of all endpoints in the API
![alt text](/Images/API.png)


# Running JAVAScript script with Node JS
The script is created to facilitate 3 activities at a time, filling the database, creating a backup and migrating all data to SQLite.

## 1. Filling the database
#### About
The data filling script is created in JAVAScript together with node JS which allows it to be executed through a command line, it is based on requests to the API to the endpoints, and then through the endpoints to obtain data, use the Ids to fill the other tables.
It was decided to create and execute these commands with `npm` because of the simplicity they offer us by being able to execute everything with a single command.
#### Example of using endpoint to create users
Connection to the endpoint <br>
![alt text](/Images/createUserEndpoint.png) <br>
Iterative user creation <br>
![alt text](/Images/createUser.png)
#### Execution of data creation in the MySQL database
To execute the data insertion, from the root directory we must move to the /FillingAndMigration directory
```
cd FillingAndMigration/
```
Then we must execute the relevant command to create all the records:
```
npm run insert
```
You will see the creation of all users, groups and tasks through the console. <br>
![alt text](/Images/creacionUsersByTerminal.png)<br>
`Note:` User creation directly uses the API endpoints, so the API must be running.
## 2. Creating the backup
Using a JAVAScript Script, the MySQL database is exported into a .sql file that maintains the entire data structure, the tables, as well as the relationships between tables. <br>
To execute the backup, from the root directory we must move to the /FillingAndMigration directory
```
cd FillingAndMigration/
```
And the command must be executed to make the backup.
```
npm run backup
```
Inside the console you will see a message that will tell us that the backup is done.
![alt text](/Images/backupDone.png)
And we can view the backup with a .sql extension within the same directory.<br>
![alt text](/Images/backupImage.png)<br>
`Note:` The command can be executed at any time as long as the database within the MySQL container is active, it is recommended that it be executed before performing a data migration to another database.

## 3. Migration to SQLite
#### type of migration
A "Big Ban" type migration was chosen in which all the data is migrated in a single execution of the tool created to move all the data and complete integration of the database.<br>
The migration is carried out with a JAVAScript script which is responsible, table by table, for making a complete copy of the structure as well as all the records entered into the database, in a later section within this same Readme.md file. You can find the queries with which you can check the integrity of the data.
#### Execution
To execute the migration, from the root directory we must move to the /FillingAndMigration directory
```
cd FillingAndMigration/
```
The following command must be executed:
```
npm run migration
```
Within the script you have all the necessary configuration, it will automatically create the file with a .db extension in the same directory.<br>
When you run the command, you will be able to see the table creation messages within the SQLite file:
![alt text](/Images/migrationCreated.png)<br>
And we can view the .db file inside the directory:<br>
![alt text](/Images/dbFile.png)<br>

# Running APIs with SQLite
Once the migration is created, we must change some lines of code to be able to execute the same API with the new database. Within this section, the exact lines that must be modified will be indicated.
## 1.RestAPI.csproj
EntityFramework does not allow the existence of more than one PackageReference of a database, so the one referring to MySQL must be commented out and the one referring to SQLite must be uncommented. When making the change on lines 17 and 18 it should look like this:<br>
![alt text](/Images/MigrationChanges1.png)<br>
## 2.Program.cs
Changes must also be made to the service of the
DataContext referencing SQLite. Lines 12 and 13 will be uncommented and code lines 15 and 16 will be commented out. When the changes are completed, it should look like this: <br>
![alt text](/Images/MigrationChanges2.png)
## 3.appsettings.Development.json
The path of our database server must be modified, which will now become a .db file <br>
`Note:` since it is a .jason file, the existing connection path must be deleted, so if you want to use MySQL again, you must remember to add the path `"Server=localhost;Port=3308;DataBase=carlos.cortes.mysql; Uid=root;Pwd=development"`.<br>
When you make the change to SQLite it should look like this:<br>
![alt text](/Images/MigrationChanges3.png)
## 4.Data/DataContext.cs
Finally, the server path must be changed within the dataContext, referring to the fact that SQLite will be used. When the changes are completed, it should look like this:
![alt text](/Images/MigrationChanges4.png)

The API is currently working with SQLite, any query or action created on the endpoints will be saved in the new database.<br>
Note: In case you have an error with the new database, you can create a new migration with the command:
```
dotnet ef migrations add NewMigration
```
To later update the database with the command:
```
dotnet ef database update
```

# Database Integrity Verification:
To verify the database, a series of queries are executed in both MySQL and SQLite, in which the results must be the same, which would correspond to a correct migration.
> Requirements:
> - In order to better view the MySQL database, it is recommended to use `MySQL Workbench`, entering the connection through port 3308, user = root, password = development.
> - To view the SQLite database and execute the queries, it is recommended to use the `DB Browser for SQLite tool`, which can be downloaded from the following [link](https://sqlitebrowser.org/dl/).
## 1.Task count query.
`SELECT COUNT(*) AS number_of_elements FROM 'Tasks'`; <br>
![alt text](/Images/Query1.png)<br>
The query returns the total number of Tasks in the database, when executed in the SQLite database it should return the same number of Tasks.<br>
![alt text](/Images/Query1.1.png)

## 2.Priority level query (2)
`SELECT COUNT(*) FROM Tasks WHERE Priority = 2;`<br>
Taking advantage of the random values in the priority column of the Tasks, a query will be executed that returns the Task number that has priority 2.
SELECT COUNT(*) FROM `Tasks` WHERE Priority = 2;<br>
![alt text](/Images/Query2.png)<br>
Result in SQLite:<br>
![alt text](/Images/Query2.1.png)<br>

## 3.Priority level query (4)
`SELECT COUNT(*) FROM Tasks WHERE Priority = 4;`<br>
Taking advantage of the random values in the priority column of the Tasks, a query will be executed that returns the Task number that has priority 4.
![alt text](/Images/Query3.png)<br>
Result of the same query in SQLite:<br>
![alt text](/Images/Query3.1.png)

## 4.Checking a specific Id in Users;
`SELECT * FROM Users WHERE Id = "5b4f9a80-3eca-40d5-9565-60f80b4331b7";` <br>
Knowing that each User has a completely different Id, we can search for a specific objective that demonstrates that exactly the same user exists in both databases.<br>
Result with MySQL:<br>
![alt text](/Images/Query4.1.png)

# Test Cases for endpoint verification
## 1.Users endpoint verification
We can create a user only by entering the name and password, complying with the restrictions that the name can only contain letters and numbers, and a size less than 25 characters, while the password must be at least 8 characters.<br>
> {<br>
>"name": "NewUser1",<br>
>"password": "12345678"<br>
>}

<br>![alt text](/Images/epUserCreate.png)
--------------------------------------------
--------------------------------------------
### Negative cases
Executing the API through the console or through a code editor such as VisualStudio Code, once you enter the interface through the explorer, you will be directed to create a new User. <br>
We enter the following information:<br>
>{<br>
>  "name": "NewUser1*",<br>
>  "password": "12345678"<br>
>}<br>

Given the restrictions, we received an error message indicating that only numbers and letters are allowed:<br>
![alt text](/Images/enUserCreateNegativeTest1.png)
-------------------------------------------------
