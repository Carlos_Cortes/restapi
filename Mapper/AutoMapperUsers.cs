using AutoMapper;
using RestAPI.Dtos.UserDtos;
using RestAPI.Models;

namespace RestAPI.Mapper
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<UserCreateDto, User>().ReverseMap();
        }
    }
}
