using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Models
{
    public class User
    {
        
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool? Status { get; set; }
        public List<Group> Groups { get; set; }
        public List<Models.Task> Tasks { get; set; }

        public User()
        {
            Groups = new List<Group>();
            Tasks = new List<Models.Task>();
            CreatedDate = DateTime.Now;
            Status = true;
        }

    }
}