using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Models
{
    public class History
    {
        public Guid Id { get; set; }
        public string AffectedEntity { get; set; }
        public Guid IdAffectedEntity { get; set; }
        public string Affectation { get; set; }
        public DateTime Date{ get; set; }
        
    }
}