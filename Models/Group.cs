using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Models
{
    public class Group
    {
        public Guid Id { get; set; }
        public string Tittle { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public List<Task> Tasks { get; set;}

        public Group()
        {
            Tasks = new List<Models.Task>();
        }
    }
}