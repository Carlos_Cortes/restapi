import mysql from 'mysql2/promise';
import fs from 'fs';

async function backupMySQLDatabase(outputFile) {
    try {
        const connection = await mysql.createConnection({
            host: 'localhost',
            port: '3308',
            user: 'root',
            password: 'development',
            database: 'carlos.cortes.mysql'
        });

        const currentDateTime = new Date().toISOString().replace(/:/g, '-');

        const tables = await connection.query("SHOW TABLES;");
        for (const table of tables[0]) {
            const tableName = table['Tables_in_carlos.cortes.mysql'];
            const createTableQuery = await connection.query(`SHOW CREATE TABLE \`${tableName}\`;`);
            const tableData = await connection.query(`SELECT * FROM \`${tableName}\`;`);

            await fs.promises.appendFile(outputFile, `\n-- Table structure for table \`${tableName}\` --\n\n`);
            await fs.promises.appendFile(outputFile, `${createTableQuery[0][0]['Create Table']};\n\n`);
            await fs.promises.appendFile(outputFile, `-- Dumping data for table \`${tableName}\` --\n\n`);

            for (const row of tableData[0]) {
                const rowValues = Object.values(row).map(value => typeof value === 'string' ? `'${value}'` : value).join(", ");
                await fs.promises.appendFile(outputFile, `INSERT INTO \`${tableName}\` VALUES (${rowValues});\n`);
            }
        }
        console.log(`Backup of database 'carlos.cortes.mysql' successfully created in '${outputFile}'`);
        
        await connection.end();
    } catch (error) {
        console.error(`Error performing backup: ${error}`);
    }
}

const outputFile = 'mysql-backup.sql';
backupMySQLDatabase(outputFile);
