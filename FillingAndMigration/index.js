import axios from 'axios';
//import backupMySQLDatabase from './backup.js';
//import { migrateDatabase } from './migration.js';

const createUser = async (username, password) => {
  try {
    const response = await axios.post('http://localhost:5135/api/User/create-user', {
      name: username,
      password: password
    });
    console.log(`User created: ${username}`);
    return response.data.id;
  } catch (error) {
    console.error(`Error creating user ${username}: ${error.message}`);
    throw error;
  }
};

const createGroup = async (userId, groupTitle) => {
  try {
    const response = await axios.post('http://localhost:5135/api/Group/create-group', {
      title: groupTitle, 
      userId: userId
    });
    console.log(`Grupo "${groupTitle}" created for user with Id: ${userId}`);
    return response.data.id; 
  } catch (error) {
    console.error(`Error creating group"${groupTitle}" for the user with Id: ${userId}: ${error.message}`);
    throw error;
  }
};

const createTask = async (userId, groupId, taskTitle, taskDescription, taskPriority) => {
  try {
    const response = await axios.post('http://localhost:5135/api/Task/create-task', {
      title: taskTitle,
      description: taskDescription,
      priority: taskPriority,
      userId: userId,
      groupId: groupId
    });
    console.log(`Task "${taskTitle}" created for user with ID: ${userId} and group with ID: ${groupId}`);
    return response.data.id; 
  } catch (error) {
    console.error(`Error creating task "${taskTitle}" for user with ID: ${userId} and the group with ID: ${groupId}: ${error.message}`);
    throw error;
  }
};

const generateUsersAndGetIds = async () => {
  const userIds = []; 
  for (let i = 1; i <= 100; i++) {
    const username = `username${i}`;
    const password = `password${i}`;
    const userId = await createUser(username, password); 
    userIds.push(userId); 
  }
  return userIds; 
};

const createGroupsForUsers = async (userIds) => {
  const groupIds = [];
  for (let i = 0; i < userIds.length; i++) {
    const userId = userIds[i];
    const groupTitle = `group${i + 1}`;
    const groupId = await createGroup(userId, groupTitle);
    groupIds.push(groupId);
  }
  return groupIds;
};

const createTasksForGroups = async (userIds, groupIds) => {
    for (let i = 0; i < groupIds.length; i++) {
      const groupId = groupIds[i];
      const userId = userIds[i];
      console.log(userId);
      for (let j = 1; j <= 20; j++) {
        const taskTitle = `task${j}`; 
        const taskDescription = `Description of task ${j}`;
        const taskPriority = Math.floor(Math.random() * 5);; 
        await createTask(userId, groupId, taskTitle, taskDescription, taskPriority); 
      }
    }
  };

  generateUsersAndGetIds()
  .then(userIds => {
    console.log('Created user IDs:', userIds);

    return createGroupsForUsers(userIds).then(groupIds => {
      return { userIds, groupIds };
    });
  })
  .then(({ userIds, groupIds }) => {
    console.log('Created group IDs:', groupIds);

    return createTasksForGroups(userIds, groupIds);
  })
  .then(() => console.log('Tasks created for all groups'))
  .catch(error => console.error('Error:', error.message));



  