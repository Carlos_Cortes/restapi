import mysql from 'mysql2/promise';
import sqlite3 from 'sqlite3';

 async function migrateDatabase() {
    try {

        const mysqlConnection = await mysql.createConnection({
        host: 'localhost',
        port: '3308',
        user: 'root',
        password: 'development',
        database: 'carlos.cortes.mysql'
        });

        const escapeTableName = (tableName) => {
        return `\`${tableName}\``;
        };

        const tableCreationOrder = ['Users', 'Groups', 'Tasks', 'History'];

        const sqliteConnection = new sqlite3.Database('carlos.cortes.sqlite.db', (err) => {
        if (err) {
            console.error('Error opening SQLite database:', err.message);
            return;
        }
        console.log('SQLite database opened successfully.');
        });

        for (const tableName of tableCreationOrder) {
        const [tableRows, tableFields] = await mysqlConnection.execute(`DESCRIBE ${escapeTableName(tableName)}`);

        let createTableSql = `CREATE TABLE IF NOT EXISTS ${escapeTableName(tableName)} (`;
        for (const tableRow of tableRows) {
            createTableSql += `${tableRow.Field} ${tableRow.Type}, `;
        }
        createTableSql = createTableSql.slice(0, -2); 
        createTableSql += ')';

        console.log('SQL to create table:', createTableSql);

        await new Promise((resolve, reject) => {
            sqliteConnection.run(createTableSql, async (sqliteErr) => {
            if (sqliteErr) {
                console.error(`Error creating table in SQLite ${tableName}: ${sqliteErr.message}`);
                reject(sqliteErr);
            } else {
                console.log(`Table created in SQLite: ${tableName}`);

                const [dataRows, dataFields] = await mysqlConnection.execute(`SELECT * FROM ${escapeTableName(tableName)}`);
                for (const dataRow of dataRows) {
                const columns = Object.keys(dataRow).join(', ');
                const placeholders = Object.keys(dataRow).map(() => '?').join(', ');
                const values = Object.values(dataRow);
                sqliteConnection.run(`INSERT INTO ${escapeTableName(tableName)} (${columns}) VALUES (${placeholders})`, values, (insertErr) => {
                    if (insertErr) console.error(`Error inserting data into SQLite for table ${tableName}: ${insertErr}`);
                });
                }
                resolve();
            }
            });
        });
        }

        await mysqlConnection.end();
        sqliteConnection.close();
    } catch (error) {
        console.error(error);
    }
}
migrateDatabase();
