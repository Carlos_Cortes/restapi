import mysql from 'mysql2/promise';

const connection = mysql.createConnection({
    host: 'localhost',
    port: '3308',
    user: 'root',
    password: 'development',
    database: 'carlos.cortes.mysql'
});

export default connection;
