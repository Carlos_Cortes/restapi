using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.HttpResults;

namespace RestAPI.Dtos.UserDtos
{
    public class UserCreateDto
{
    private string _name;
    private string _password;

    public string Name
    {
        get { return _name; }
        set
        {
            if (!Regex.IsMatch(value, "^[a-zA-Z0-9]+$"))
            {
                throw new ArgumentException("Name can only contain letters and numbers.");
            }

            if (value.Length > 25)
            {
                throw new ArgumentException("Name cannot be longer than 25 characters.");
            }

            _name = value;
        }
    }

    public string Password
    {
        get { return _password; }
        set
        {
            if (value.Length < 8)
            {
                throw new ArgumentException("Password must be at least 8 characters long.");
            }

            _password = value;
        }
    }
}
}