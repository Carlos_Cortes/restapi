using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Dtos.TaskDtos
{
    public class TaskUpdateFinishDate
    {
        public Guid Id { get; set; }
        public DateTime FinishDate { get; set;}
    }
}