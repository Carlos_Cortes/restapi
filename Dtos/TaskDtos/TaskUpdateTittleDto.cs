using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Dtos.TaskDtos
{
    public class TaskUpdateTittleDto
    {
        public Guid Id { get; set; }
        public string Tittle { get; set; }
    }
}