using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RestAPI.Dtos.TaskDtos
{
    public class TaskCreateDto
    {
        private string _title;
        private string _description;
        private int _priority;
        public string Title 
        { 
            get {return _title; }
            set
            {
                if (!Regex.IsMatch(value, "^[a-zA-Z0-9]+$"))
                {
                    throw new ArgumentException("Title can only contain letters and numbers.");
                }

                if (value.Length > 25)
                {
                    throw new ArgumentException("Title cannot be longer than 25 characters.");
                }

                _title = value;
            }
        }
        public string Description 
        { 
            get {return _description; }
            set
            {
                if (value.Length > 300)
                {
                    throw new ArgumentException("Description cannot be longer than 300 characters.");
                }

                _description = value;
            }
        }
        public int Priority 
        { 
            get { return _priority; }
            set
            {
                if (value < 0 || value > 4)
                {
                    throw new ArgumentException("Priority must be between 0 and 4.");
                }

                _priority = value;
            }
        }
        public Guid UserId { get; set; }
        public Guid? GroupId { get; set; }
    }
}