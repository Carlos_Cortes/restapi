using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Dtos.TaskDtos
{
    public class TaskReturnDto
    {
         public Guid Id { get; set; }
        public string Tittle { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set;}
        public bool Status { get; set; }
        public DateTime? FinishDate { get; set;}
        public int? Priority {get; set; }
        public Guid UserId { get; set; }
        public Guid? GroupId { get; set; }
    }
}