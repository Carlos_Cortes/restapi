using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Dtos.GroupDtos
{
    public class GroupReturnDto
    {
        public Guid Id { get; set; }
        public string Tittle { get; set; }
        public Guid UserId { get; set; }
    }
}