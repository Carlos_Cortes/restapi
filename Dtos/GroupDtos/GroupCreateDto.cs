using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RestAPI.Dtos.GroupDtos
{
    public class GroupCreateDto
    {
        private string _title;
        public string Title 
        { 
            get {return _title; }
            set
            {
                if (!Regex.IsMatch(value, "^[a-zA-Z0-9]+$"))
                {
                    throw new ArgumentException("Title can only contain letters and numbers.");
                }

                if (value.Length > 25)
                {
                    throw new ArgumentException("Title cannot be longer than 25 characters.");
                }

                _title = value;
            }
        }
        public Guid UserId { get; set; } 
    }
}